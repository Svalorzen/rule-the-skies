#ifndef __SLOT_TEMPLATE_HEADER
#define __SLOT_TEMPLATE_HEADER

#include "slot_types.h"
#include <array>

namespace Json { class Value; }

class SlotTemplate {
	public:
		SlotTemplate( const Json::Value & _slot );
		SlotTemplate( Directions _dirs, int _maxHp, bool _ac );
		SlotTemplate( int _dirs, int _maxHp, bool _ac );
		SlotTemplate( const SlotTemplate & _other );

		bool operator==(const SlotTemplate & other) const;

		Json::Value convertToJson() const;

		// Sets properties of slots.
		void setDirections ( Directions _dir );
		void setDirections ( int _dir );
		void setMaxHp( int _hp );
		void setAllowedCannons( bool _ac );
		void toggleAllowedCannons();

		// Inverts direction bits
		void invertDirections();

		// SUMMARIES ------------

		// Dirs [0,0,0], Cannons?, Cost
		std::array<std::string, 3> getSummary() const;

		// GETTERS --------------

		int getMaxHp() const;
		int getCost() const;
		bool getAllowedCannons() const;
		Directions getDirections() const;

		static bool compare( const SlotTemplate & a, const SlotTemplate & b );

	private:
		Directions dirs;
		int maxHp;
		bool allowedCannons;
		int cost;
};

#endif
