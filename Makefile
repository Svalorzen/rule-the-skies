SHELL=/bin/bash
CC=g++
EXECUTABLE=ruletheskies

CFLAGS=-std=c++0x -Wall -pedantic #-I/usr/include/boost_1_51_0/
LDFLAGS=-L./
LIBRARIES=-ljson_linux-gcc-4.6_libmt -lbprinter
WINLIBRARIES=-ljson_mingw_libmt -lbprinter_mingw

WINDEFINES=-DWINVER=0x0400 -D__WIN95__ -D__GNUWIN32__ -DSTRICT -DHAVE_W32API_H -D__WXMSW__ -D__WINDOWS__
WINLDFLAGS=-I./ # $(shell find /usr/include/ -maxdepth 1 -type d | sed 's/^/-I/g') #-L/usr/lib/ -I/usr/include/
WINBASELIBRARIES=-lodbc32 -lwsock32 -lwinspool -lwinmm -lshell32 -lcomctl32 -lctl3d32 -lodbc32 -ladvapi32 -lodbc32 -lwsock32 -lopengl32 -lglu32 -lole32 -loleaut32 -luuid
WINSTATIC=-static-libstdc++ -static-libgcc
WINOPTIONS=--verbose

SOURCES := $(wildcard *.cpp)
HEADERS := $(wildcard *.h)

OBJECTSFOLDER=objects
OBJECTS=$(addprefix $(OBJECTSFOLDER)/, $(SOURCES:.cpp=.o))

DEPFOLDER=dependencies
df=$(DEPFOLDER)/$(*F)
odf=$(OBJECTSFOLDER)/$(*F)

EXEFOLDER=bin

.PHONY : all
# Brute force compiling statement for simple compilation and no .o residual files
all: $(EXEFOLDER)
	$(CC) $(CFLAGS) $(LDFLAGS) *.cpp $(LIBRARIES) -o $(EXEFOLDER)/$(EXECUTABLE)

.PHONY: debug
# Compiles using object files in OBJECTSFOLDER
debug: folders $(SOURCES) $(EXEFOLDER)/$(EXECUTABLE)

.PHONY: windows
# cross-compilation: mingw has to be selected, adds libraries for windows
windows: $(EXEFOLDER)
	$(CC) $(CFLAGS) $(WINDEFINES) $(WINSTATIC) $(LDFLAGS) $(WINLDFLAGS) *.cpp $(LIBRARIES) $(WINLIBRARIES) $(WINBASELIBRARIES) -o $(EXEFOLDER)/$(EXECUTABLE).exe $(WINOPTIONS)
	
# Final linking of the executable
$(EXEFOLDER)/$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(LIBRARIES) -o $@

# Creation of object files
# ( Actual compilation is only last instruction, the rest is creating dependency header rules )
$(OBJECTSFOLDER)/%.o: %.cpp
	@$(CC) -MMD -c $(CFLAGS) -o $@ $<
	@cp $(odf).d $(df).P; \
		sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
			-e '/^$$/ d' -e 's/$$/ :/' < $(odf).d >> $(df).P; \
	rm -f $(odf).d
	$(CC) -c $(CFLAGS) $< -o $@

-include $(DEPFOLDER)/*.P

# Creation of folders
.PHONY : folders
folders: $(OBJECTSFOLDER) $(DEPFOLDER) $(EXEFOLDER)
$(OBJECTSFOLDER):
	mkdir $@
$(DEPFOLDER):
	mkdir $@
$(EXEFOLDER):
	mkdir $@

# Cleanup of object files
# ( .PHONY is to avoid calling the rule in case a file named clean exists )
.PHONY : clean
clean:
	rm -rf $(OBJECTSFOLDER)
	rm -rf $(DEPFOLDER)
