#ifndef __TEMPLATE_SET_HEADER
#define __TEMPLATE_SET_HEADER

#include <list>
#include <vector>
#include <array>
#include <string>

class ShipTemplate;
namespace Json { class Value; }

class TemplateSet {
	public:
		TemplateSet(std::string _name = "");
		TemplateSet(const Json::Value & _set, std::string _name = "");

		Json::Value convertToJson() const;

		// Adds a Ship template to the Set
		void addShipTemplate( const ShipTemplate & _shipTemplate );
		void addShipTemplate( const Json::Value & _shipTemplate, std::string _name = "" );

		// Removes a Ship template from the Set
		void rmShipTemplate( int _shipTemplate );

		void replShipTemplate( int _shipTemplate, const ShipTemplate & _newShipTemplate );

		void renameShipTemplate( int _shipTemplate, std::string _name );

		// SUMMARIES ------------------

		// Returns the summaries of all the Ship Templates 
		std::vector<std::array<std::string,3>> getShipTemplateSummaries() const;
		// Returns the names of all the Ship Templates
		std::vector<std::string> getShipTemplateNames() const;

		// GETTERS --------------------

		// Returns a constant pointer to a Ship template
		const ShipTemplate * getShipTemplate( int _shipTemplate ) const;
		// Returns the number of Ship templates in this Set	
		int getShipTemplatesNumber() const;
		// Returns if the Set contains a given Template
		bool containsShipTemplate( const ShipTemplate & _shipTemplate ) const;

		// The name of this Template Set
		std::string name;
	private:
		// The templates in this set
		// We can't use vectors because ShipTemplates don't have operator=
		// which is needed for erase() operations
		std::list<ShipTemplate> shipTemplates;
};

#endif
