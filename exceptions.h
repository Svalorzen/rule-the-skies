#ifndef EXCEPTIONS_HEADER_FILE
#define EXCEPTIONS_HEADER_FILE

#include <stdexcept>

// This part expands the LINE macro from an int to a string ( magic )
#define S(x) #x
#define S_(x) S(x)
#define S_LINE S_(__LINE__)
#define DEBUGEXC "Exception occurred at: " S_LINE " " __FILE__ "\n"

#endif
