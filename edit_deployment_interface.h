#ifndef __EDITDEPLOYMENTINTERFACE_HEADER
#define __EDITDEPLOYMENTINTERFACE_HEADER

#include "fsm.h"

class MainDeploymentIntro : public FSMState {
	public:
		MainDeploymentIntro( FSM * _fsm );
		
		virtual int update();
};

class InspectDeployment : public FSMState {
	public:
		InspectDeployment( FSM * _fsm );

		virtual void doEnter(int param);

		virtual int update();
};

class InspectFleet : public FSMState {
	public:
		InspectFleet( FSM * _fsm );

		virtual void doEnter(int param);

		virtual int update();
	private:
		int selectedFleet;
};

class InspectShip : public FSMState {
	public:
		InspectShip( FSM * _fsm );

		virtual void doEnter(int param);

		virtual int update();
	private:
		int selShip;
		int selFleet;
};

#endif
