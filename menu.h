#ifndef __MENU_HEADER
#define __MENU_HEADER

#include <limits>
#include <vector>
#include <string>

namespace Menu {

	int getValidInt(int _min, int _max);
	int print( const std::vector<std::string> & _options, bool _back, std::string incipit = "" );
}

#endif
