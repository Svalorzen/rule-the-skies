//#include "map.h"
#include "edit_template_interface.h"
#include "edit_deployment_interface.h"

#include <iostream>
using namespace std;

int main() {
	cout << "\nWelcome to the Fleet Builder v1.0 for the \"Rule The Skies(tm)\" ruleset!\n\n";

	FSM * interface = new FSM();
	
	MainDeploymentIntro * introD = new MainDeploymentIntro( interface );
	InspectDeployment * inspectD = new InspectDeployment( interface );
	MainTemplateIntro * introT = new MainTemplateIntro( interface );
	InspectTemplateSet * inspectTS = new InspectTemplateSet( interface );
	InspectShipTemplate * inspectST = new InspectShipTemplate( interface );
	EditShipTemplate * editST = new EditShipTemplate( interface );
	InspectFleet * inspectF = new InspectFleet( interface );
	InspectShip * inspectS = new InspectShip( interface );

	interface->addState(introD, true);
	interface->addState(inspectD, false);
	interface->addState(introT, false);
	interface->addState(inspectTS, false);
	interface->addState(inspectST, false);
	interface->addState(editST, false);
	interface->addState(inspectF, false);
	interface->addState(inspectS, false);

	// Main loop
	while ( interface->update() != -1 ) {}

	// This will also delete all states and the Deployment
	delete interface;

	return 0;
}
