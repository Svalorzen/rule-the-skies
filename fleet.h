#ifndef __FLEET_HEADER
#define __FLEET_HEADER

#include <vector>
#include <array>
#include <string>
#include "cost_classes.h"

class Ship;
namespace Json { class Value; }

class Fleet : public CostKeeper {
	public:
		// Constructor
		Fleet(std::string _name);

		Fleet(const Json::Value & _fleet, std::string _name = "");

		// Creates Json equivalent
		Json::Value convertToJson(bool _shipsToo = true) const;

		// Adds a Ship and updates the Fleet's cost
		void addShip(Ship * _ship);

		// Removes a Ship from the Fleet ( does not delete it ) and updates the Fleet's cost
		void rmShip(int _ship);
		void rmShip(Ship * _ship);

		// SUMMARIES ----------------

		// Name, Captain, ShipsNumber, Cost
		std::array<std::string,4> getSummary() const;
		std::vector<std::array<std::string,3>> getShipSummaries() const;

		// GETTERS -------------------

		const Ship* getShip(int _ship) const;
		int getShipsNumber() const;
		std::vector<std::string> getShipNames() const;

		std::string name;
		std::string captain;
	private:
		std::vector<Ship*> ships;
};

#endif
