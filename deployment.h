#ifndef __DEPLOYMENT_HEADER
#define __DEPLOYMENT_HEADER

#include "cost_classes.h"
#include <tuple>
#include <string>
#include <vector>
#include <array>

class TemplateSet;
class ShipTemplate;
class Fleet;
class Ship;
namespace Json { class Value; }

class Deployment : public CostKeeper {
	public:
		Deployment( std::string _name = "" );
		Deployment( const Json::Value & _dep, std::string _name = "" );
		~Deployment();

		Json::Value convertToJson() const;

		// FLEET FUNCTIONS
		void addFleet( std::string _fleetName = "" );
		void rmFleet( int _fleet, bool _delete = false );

		void renameFleet( int _fleet, std::string _name );

		// SHIP FUNCTIONS
		int shipFleetIndex( int _ship, int _fleet );

		void addShip( std::string _name, int _thrust, const ShipTemplate & _template, int _fleet = 0 );

		void rmShip( int _ship );
		void rmShip( int _ship, int _fleet );

		void mvShip( int _ship, int _toFleet );
		void mvShip( int _ship, int _fromFleet, int _toFleet );

		void renameShip( int _ship, std::string _name );
		void renameShip( int _ship, int _fleet, std::string _name );

		// SHIP LOADOUT FUNCTIONS

		void editShipLoadout( int _ship, int _side, int _slot, int _loadout );
		void editShipLoadout( int _ship, int _fleet, int _side, int _slot, int _loadout );

		void editShipTower( int _ship, int _tower, int _loadout );
		void editShipTower( int _ship, int _fleet, int _tower, int _loadout );

		// SUMMARIES -------------

		std::vector<std::array<std::string,4>> getShipSummaries() const;

		std::vector<std::array<std::string,4>> getFleetSummaries() const;

		std::vector<std::array<std::string,3>> getShipTemplateSummaries() const;

		// GETTERS ---------------

		const Ship * getShip( int _ship ) const;
		const Ship * getShip( int _ship, int _fleet );
		const ShipTemplate * getShipTemplate(int _template) const;
		const Fleet * getFleet(int _fleet) const;

		int getShipsNumber() const;
		int getFleetsNumber() const;
		int getRealFleetsNumber() const;

		std::vector<std::string> getShipNames() const;
		std::vector<std::string> getFleetNames() const;
		std::vector<std::string> getRealFleetNames() const;
		std::vector<std::string> getShipTemplateNames() const;

		std::string name;
		TemplateSet * ownTemplateSet;
	protected:
		// TEMPLATE FUNCTIONS
		void addShipTemplate( ShipTemplate * _template, Ship * _ship);

	private:
		std::vector<std::tuple<ShipTemplate*, std::vector<Ship*>>> templates;
		std::vector<std::tuple<Ship*, Fleet*>> ships;
		std::vector<Fleet*> fleets;
};

#endif
