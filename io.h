#ifndef INPUTOUTPUT_HEADER_FILE
#define INPUTOUTPUT_HEADER_FILE

#include "json/value.h"
#include "exceptions.h"

class SlotTemplate;
class ShipTemplate;
class TemplateSet;
class Deployment;

class FiletypeError : public std::runtime_error {
	public:
		explicit FiletypeError(const std::string & what_arg);
};

namespace IO {
	Json::Value loadJson( std::string filename_ );
	// Saves a SLOT TEMPLATE
	Json::Value saveTemplate( const SlotTemplate & slot_ );

	// Saves a SHIP TEMPLATE
	Json::Value saveTemplate( const ShipTemplate & ship_ );

	// Saves a TEMPLATE SET
	Json::Value saveTemplate( const TemplateSet & set_ );

	// Loads one of the STANDARD SHIP TEMPLATES
	Json::Value loadStandardShipTemplate( int shipTemplateType_ );

	// Loads a TEMPLATE SET
	Json::Value loadTemplateSet( std::string filename_, std::string setName_ = "" );

	// Loads a DEPLOYMENT
	Json::Value loadDeployment( std::string filename_, std::string depName_ = "" );

}

#endif
