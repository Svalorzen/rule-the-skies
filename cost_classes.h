#ifndef COST_CLASSES_HEADER
#define COST_CLASSES_HEADER

#include <vector>

class CostChanger {
	public:
		CostChanger(int _cost = 0);
		~CostChanger();

		int getCost() const;
		
		void registerObserver( CostChanger * _obs );
		void unregisterObserver( CostChanger * _obs );
	protected:
		void setCost(int _cost);
		void addCost(int _diffCost);

	private:
		void notifyObservers(int _costDiff);
		void notify(int _costDiff);

		void registerObserved( CostChanger * _obs );
		void unregisterObserved( CostChanger * _obs );

		int cost;
		std::vector<CostChanger*> observerCollection;
		std::vector<CostChanger*> observedCollection;

};

class CostKeeper : public CostChanger {
	public:
		CostKeeper(int _cost);
	private:
		using CostChanger::setCost;
		using CostChanger::addCost;
};

#endif
