#ifndef __SHIP_TYPES_HEADER
#define __SHIP_TYPES_HEADER

#include <stdexcept>
#include <string>

const int MIN_SIDE = 0;
const int MAX_SIDE = 5;
const int ALL_SIDES = 6;

const int MIN_SHIP_CLASS = 0;
const int LIGHT_SHIP_CLASS = 0;

const int MEDIUM_SHIP_CLASS = 1;

const int HEAVY_SHIP_CLASS = 2;
const int MAX_SHIP_CLASS = 2;


inline std::string STRING_CLASS( int _class ) {
	if ( _class < MIN_SHIP_CLASS || _class > MAX_SHIP_CLASS ) throw std::invalid_argument("inline STRING_CLASS(int)");

	std::string classes[3] = { "Light Class", "Medium Class", "Heavy Class" };

	return classes[_class];
}


const int MIN_SHIP_TYPE = 0;
const int STANDARD_LIGHT_SHIP = 0;

const int STANDARD_MEDIUM_SHIP = 1;

const int STANDARD_HEAVY_SHIP = 2;
const int MAX_SHIP_TYPE = 2;



inline int HP_SHIP_COST( int _hp ) {
	if ( _hp < 1 ) throw std::invalid_argument("inline HP_SHIP_COST(int)");

	return 10*_hp;
}



const int MIN_DIRECTION = 0;
const int DIRECTION_NORTH_EAST = 0;

const int DIRECTION_EAST = 1;
const int DIRECTION_SOUTH_EAST = 2;
const int DIRECTION_SOUTH_WEST = 3;
const int DIRECTION_WEST = 4;

const int DIRECTION_NORTH_WEST = 5;
const int MAX_DIRECTION = 5;



const int MIN_THRUST = 1;
const int MAX_THRUST = 3;



const int MIN_TOWER = 0;
const int NO_TOWER = 0;

const int RADAR_TOWER = 1;
const int COMPUTING_TOWER = 2;

const int TRACKING_TOWER = 3;
const int MAX_TOWER = 3;



// Must return positive values
inline int MAX_SPEED( int _thrust ) {
	if ( _thrust < MIN_THRUST ||
		 _thrust > MAX_THRUST ) throw std::invalid_argument("inline MAX_SPEED(int)");

	switch ( _thrust ) {
		case 1:
			return 3;
		case 2:
			return 3;
		case 3:
			return 4;
	}

	throw std::runtime_error("inline MAX_SPEED(int)");
}

inline int TOWER_SLOT_COST( int _towers ) {
	if ( _towers < 0 ) throw std::invalid_argument("inline TOWER_SLOT_COST(int)");

	return _towers*20;
}

inline int TOWER_COST( int _tower ) {
	if ( _tower < MIN_TOWER ||
		 _tower > MAX_TOWER ) throw std::invalid_argument("inline TOWER_COST(int)");

	switch ( _tower ) {
		case NO_TOWER:
			return 0;
		case RADAR_TOWER:
			return 40;
		case COMPUTING_TOWER:
			return 45;
		case TRACKING_TOWER:
			return 80;
	}

	throw std::runtime_error("inline TOWER_COST(int)");
}

inline std::string STRING_TOWER( int _tower ) {
	if ( _tower < MIN_TOWER ||
		 _tower > MAX_TOWER ) throw std::invalid_argument("inline STRING_TOWER(int)");

	std::string names[4] = { "None", "Radar", "Computing", "Tracking" };

	return names[_tower];
}

// Must be positive
const int MAX_HEIGHT = 3;

inline std::string STRING_SIDE( int _side ) {
	if ( _side < MIN_SIDE || _side > MAX_SIDE ) throw std::invalid_argument("inline STRING_SIDE(int)");

	std::string sides[6] = { "FRONT", "FRONT RIGHT", "BACK RIGHT", "BACK", "BACK LEFT", "FRONT LEFT" };

	return sides[_side];
}


#endif
