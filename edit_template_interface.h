#ifndef __EDITTEMPLATEINTERFACE_HEADER
#define __EDITTEMPLATEINTERFACE_HEADER

#include "fsm.h"

class MainTemplateIntro : public FSMState {
	public:
		MainTemplateIntro( FSM * _fsm );
		virtual int update();
};

class InspectTemplateSet : public FSMState {
	public:
		InspectTemplateSet( FSM * _fsm );
		virtual void doEnter(int param);
		virtual int update();
};

class InspectShipTemplate : public FSMState {
	public:
		InspectShipTemplate( FSM * _fsm );
		virtual void doEnter(int param);
		virtual int update();
	private:
		int selectedShip;
};

class EditShipTemplate : public FSMState {
	public:
		EditShipTemplate( FSM * _fsm );
		virtual void doEnter(int param);
		virtual int update();
	private:
		int selectedShip;
};

#endif
