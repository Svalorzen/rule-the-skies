#ifndef __TABLE_HEADER
#define __TABLE_HEADER

class TemplateSet;
class ShipTemplate;
class Deployment;
class Fleet;
class Ship;

namespace Table {

	void print( const TemplateSet& _set );

	void print( const ShipTemplate& _ship );

	void print( const Ship& _ship );

	void printStat( const Ship& _ship );

	void print( const Deployment& _dep );

	void print( const Fleet& _fleet );

}

#endif
