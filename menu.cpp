#include "menu.h"
#include <iostream>
#include <limits>

using namespace std;

namespace Menu {
	int getValidInt(int _min, int _max) {
		int number;

		while ( !(cin >> number) || (number < _min) || (number > _max) || cin.peek() != '\n') {
			cout << "\nYou must input a whole number between " << _min << " and " << _max << "\n\n";
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		cout << "\n";
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		return number;
	}

	int print( const vector<string> & _options, bool _back, string incipit ) {
		int rangeMin = 1;
		int rangeMax = _options.size();

		if ( _back ) {
			cout << "Enter 0 to return to the previous menu;\n";
			rangeMin = 0;
		}
		if ( incipit != "" ) incipit += " ";

		int counter = 1;
		for ( vector<string>::const_iterator it = _options.begin(); it != _options.end(); it++ ) {
			cout << "Enter " << counter << " to " << incipit << *it << ";\n"; 
			counter++;
		}

		cout << "\n";

		return getValidInt(rangeMin, rangeMax);
	}

}
